#include <math.h>
#include <iostream>
#include "graphics.h"
#include "grafika\functions.h"
#include "../grafika/grafika/State.h"
#include "grafika\State.h"

void draw_menu(void)
{
 int x;
 for (x=0;x<256;x++) { setcolor(COLOR(x,255,0)); line(x,0,x,30);}
 for (x=0;x<256;x++) { setcolor(COLOR(255,255-x,0)); line(255+x,0,255+x,30);}
 for (x=0;x<256;x++) { setcolor(COLOR(255,0,x)); line(510+x,0,510+x,30);}
 for (x=0;x<256;x++) { setcolor(COLOR(0,255,255-x)); line(255-x,30,255-x,60);}
 for (x=0;x<256;x++) { setcolor(COLOR(0,x,255)); line(510-x,30,510-x,60); }
 for (x=0;x<256;x++) { setcolor(COLOR(255-x,0,255)); line(765-x,30,765-x,60);}

 setcolor(COLOR(255,255,255));
 outtextxy(5,600,"f - wyb�r koloru rysowania");
 outtextxy(5,615,"b - wyb�r koloru wype�niania");
 outtextxy(5,630,"l - rysowanie linii");

 outtextxy(200,600,"r - rysowanie prostok�ta");
 outtextxy(200,615,"a - rysowanie wype�nionego prostok�ta");
 outtextxy(200,630,"c - rysowanie okr�gu");

 outtextxy(470,600,"w - zapis do pliku");
 outtextxy(470,615,"o - odczyt z pliku");
 outtextxy(470,630,"esc - wyj�cie");

 outtextxy(650,615,"Aktualne:");

 setcolor(COLOR(255,255,255));
 rectangle(1,62,798,598);
}

int main( )
{
	

	initwindow(800, 700);
	draw_menu();

	

	char keyboard_key = getch();
	char* keyboard_key_ptr = &keyboard_key;
	State::setCurrentMode(keyboard_key);

	while (keyboard_key != char(27)) {
		
		outtextxy(720, 615, keyboard_key_ptr);
	
		switch (keyboard_key) {
		case 'f':
			chooseDrawColour();
			break;
		case 'b':
			chooseBackgroundColour();
			break;
		case 'l':
			drawShape();
			break;
		case 'r':
			drawShape();
			break;
		case 'a':
			drawShape();
			break;
		case 'c':
			drawShape();
			break;
		case 'w':
			writeToFile();
			break;
		case 'o':
			readFromFile();
			break;
		}
		keyboard_key = getch();
		State::setCurrentMode(keyboard_key);
	}

	closegraph();
 return( 0 );
}

