#pragma once

#include "State.h"




void chooseDrawColour();
void chooseBackgroundColour();
void drawShape();
void writeToFile();
void readFromFile();

void mouseClickToColor(int, int);
void mouseClickToFillColor(int, int);
void mouseClickedLine(int x, int y);
void mouseMovedLine(int x, int y);
