#pragma once

class State {
private:
	static unsigned penColor;
	static unsigned fillColor;
	static char currentMode;
	static bool wasClicked;
	static unsigned x1, y1;
public:

	static void setPenColor(const unsigned& col) { penColor = col; }
	static unsigned& getPenColor() { return penColor; }
	static void setFillColor(const unsigned& col) { fillColor = col; }
	static unsigned& getFillColor() { return fillColor; }
	static void setCurrentMode(const char& mode) { currentMode = mode; }
	static char getCurrentMode() { return currentMode; }
	static void setWasClicked(const bool& val) { wasClicked = val; }
	static bool getWasClicked() { return wasClicked; }
	static void setX1(const unsigned& val) { x1 = val; }
	static unsigned getX1() { return x1; }
	static void setY1(const unsigned& val) { y1 = val; }
	static unsigned getY1() { return y1; }
};
