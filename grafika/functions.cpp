#include "../graphics.h"
#include "functions.h"
#include "State.h"
#include <math.h>
#include <iostream>




void chooseDrawColour() {
	registermousehandler(WM_LBUTTONDOWN, mouseClickToColor);
}
void chooseBackgroundColour() {
	registermousehandler(WM_LBUTTONDOWN, mouseClickToFillColor);
}
void drawShape() {
	registermousehandler(WM_LBUTTONDOWN, mouseClickedLine);
	registermousehandler(WM_MOUSEMOVE, mouseMovedLine);
}

void writeToFile() {
	writeimagefile(NULL, 3, 63, 786, 595);
}
void readFromFile() {
	readimagefile(NULL, 3, 63, 786, 595);
}

/////////////////////////////

void mouseClickedLine(int x, int y) {
	if (State::getCurrentMode() == 'l'  ) {
		if (State::getWasClicked() == false) {
			setcolor(State::getPenColor());
			State::setX1(x);
			State::setY1(y);
			State::setWasClicked(true);
			writeimagefile("cpy",0,0,800,650);
		}
		else {
			line(State::getX1(), State::getY1(), x, y);
			State::setWasClicked(false);
			setcolor(COLOR(255, 255, 255));
		}

	}
	else if (State::getCurrentMode() == 'r') {
		if (State::getWasClicked() == false) {
			setcolor(State::getPenColor());
			State::setX1(x);
			State::setY1(y);
			State::setWasClicked(true);
			writeimagefile("cpy", 0, 0, 800, 650);
		}
		else {
			rectangle(State::getX1(), State::getY1(), x, y);
			State::setWasClicked(false);
			setcolor(COLOR(255, 255, 255));
		}
	}
	else if (State::getCurrentMode() == 'a') {
		if (State::getWasClicked() == false) {
			setcolor(State::getPenColor());
			setfillstyle(SOLID_FILL, State::getFillColor());
			State::setX1(x);
			State::setY1(y);
			State::setWasClicked(true);
			writeimagefile("cpy", 0, 0, 800, 650);
		}
		else {
			bar(State::getX1(), State::getY1(), x, y);
			rectangle(State::getX1(), State::getY1(), x, y);
			State::setWasClicked(false);
			setcolor(COLOR(255, 255, 255));
		}
	}
	else if (State::getCurrentMode() == 'c') {
		if (State::getWasClicked() == false) {
			setcolor(State::getPenColor());

			State::setX1(x);
			State::setY1(y);
			State::setWasClicked(true);
			writeimagefile("cpy", 0, 0, 800, 650);
		}
		else {
			circle(State::getX1(), State::getY1(), State::getX1() - x);
			State::setWasClicked(false);
			setcolor(COLOR(255, 255, 255));
		}

	}

}

void mouseMovedLine(int x, int y) {
	if (State::getCurrentMode() == 'l'  ) {
		if (State::getWasClicked() == true) {
			readimagefile("cpy", 0, 0, 800, 650);
			line(State::getX1(),State::getY1(),x, y);
		}
	}
	else if ( State::getCurrentMode() == 'r') {
		if (State::getWasClicked() == true) {
			readimagefile("cpy", 0, 0, 800, 650);
			rectangle(State::getX1(), State::getY1(), x, y);
		}

	}
	else if (State::getCurrentMode() == 'a') {
		if (State::getWasClicked() == true) {
			readimagefile("cpy", 0, 0, 800, 650);
			bar(State::getX1(), State::getY1(), x, y);
			rectangle(State::getX1(), State::getY1(), x, y);
		}

	}
	else if (State::getCurrentMode() == 'c') {
		if (State::getWasClicked() == true) {
			readimagefile("cpy", 0, 0, 800, 650);
			circle(State::getX1(), State::getY1(), State::getX1() - x);
		}

	}
}

void mouseClickToColor(int x, int y) {
	if (State::getCurrentMode() == 'f') {
		std::cout << x << " " << y << "\n";
		int def_color = getcolor();

		if (y < 30) {
			if (x < 256) setcolor(COLOR(x, 255, 0));
			else if (x < 511) {
				x -= 255;
				setcolor(COLOR(255, 255 - x, 0));
			}
			else {
				x -= 510;
				setcolor(COLOR(255, 0, x));
			}
		}
		else if (y < 60) {
			if (x < 256) setcolor(COLOR(0, 255, 255 - (255 - x)));

			else if (x < 511) {
				x -= 255;
				setcolor(COLOR(0, 255 - x, 255));
			}
			else {
				x -= 510;
				setcolor(COLOR(255 - (255 - x), 0, 255));
			}



		}
		else setcolor(COLOR(255, 255, 255));


		rectangle(767, 1, 799, 29);
		for (unsigned i = 0; i < 799 - 767; i++) {
			line(767 + i, 1, 767 + i, 29);
		}

		State::setPenColor(getcolor());
		setcolor(def_color);
	}
	
}

void mouseClickToFillColor(int x, int y) {
	if (State::getCurrentMode() == 'b') {
		int def_bk_col = getbkcolor();
		int def_col = getcolor();

		if (y < 30) {
			if (x < 256) setbkcolor(COLOR(x, 255, 0));
			else if (x < 511) {
				x -= 255;
				setbkcolor(COLOR(255, 255 - x, 0));
			}
			else {
				x -= 510;
				setbkcolor(COLOR(255, 0, x));
			}
		}
		else if (y < 60) {
			if (x < 256) setbkcolor(COLOR(0, 255, 255 - (255 - x)));

			else if (x < 511) {
				x -= 255;
				setbkcolor(COLOR(0, 255 - x, 255));
			}
			else {
				x -= 510;
				setbkcolor(COLOR(255 - (255 - x), 0, 255));
			}



		}
		else setbkcolor(COLOR(255, 255, 255));

		State::setFillColor(getbkcolor());
		setcolor(getbkcolor());
		rectangle(767, 30, 799, 61);
		for (unsigned i = 0; i < 799 - 767; i++) {
			line(767 + i, 30, 767 + i, 61);
		}
		setcolor(def_col);
		setbkcolor(def_bk_col);
	}
}



